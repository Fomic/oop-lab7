﻿using System;
using System.Text;
using static System.Console;

namespace SimpleClassConlsole
{
    public class Product
    {
        protected string Name; 
        protected double Price; 
        protected Currency Cost;
        protected int Quantity; 
        protected string Producer; 
        protected double Weight; 
        public Product(string name, double price, string nameCurrency, double exRate, int quantity, string producer, double weight)
        { 
            Name = name;
            if (price > 0)
                Price = price;
            Currency cost = new Currency(nameCurrency, exRate);
            Cost = cost;
            if (quantity >= 0)
                Quantity = quantity;
            Producer = producer;
            if (weight >= 0)
                Weight = weight;
        }
        public Product(string name, double price, int quantity, string producer,double weight)
        {
            Name = name;
            if (price > 0)
                Price = price;
            if (quantity >= 0)
                Quantity = quantity;
            Producer = producer;
            if (weight >= 0)
                Weight = weight;
        }
        public double GetPriceInUAH()
        { 
            return Price * Cost.GetExRate();
        }
        public double GetTotalPriceInUAH()
        {
        return Price * Quantity * Cost.GetExRate();
        }
        public double GetTotalWeight()
        {
            return Weight * Quantity;
        }
        public string GetName()
        {
            return Name;
        }
        public double GetPrice()
        {
            return Price;
        }
        public int GetQuantity()
        {
            return Quantity;
        }
        public string GetCurrencyName()
        {
            return Cost.GetCurrencyName();
        }
        public double GetCurrencyExRate()
        {
            return Cost.GetCurrencyExRate();
        }
        public string GetProducer()
        {
            return Producer;
        }
        public double GetWeight()
        {
            return Weight;
        }
    }
    public class Currency
    {
        protected string Name; 
        protected double ExRate; 
        public Currency(string name, double exRate)
        {
            Name = name;
            if (exRate > 0)
                ExRate = exRate;
        }
        public string GetCurrencyName()
        {
            return Name;
        }
        public double GetCurrencyExRate()
        {
            return ExRate;
        }
        public double GetExRate()
        {
            return ExRate;
        }
    }
    public class Program
    {
        static void CheckDouble(out double x, int min = 0, int max = 200)
        {
            bool ok;
            do
            {
                ok = double.TryParse(ReadLine(), out x);
                if (!ok || x < min)
                    WriteLine("Некоректне введення! Повторіть спробу.");
            } while (!ok || x < min);
        }
        static void CheckInt(out int x, int min = 0, int max = 200)
        {
            bool ok;
            do
            {
                ok = int.TryParse(ReadLine(), out x);
                if (!ok || x < min)
                    WriteLine("Некоректне введення! Повторіть спробу.");
            } while (!ok || x < min);
        }
        static Product[] ReadProductsArray()
        {
            Write("Введіть масив продуктів\nКількість продуктів = ");
            int n; CheckInt(out n);
            Product[] array = new Product[n];
            for (int i = 0; i < n; i++)
            {
                ForegroundColor = ConsoleColor.Red;
                WriteLine($"\n{i + 1}");
                Write("Назва товару: ");
                string name = ReadLine();
                Write("Вартість одиниці товару: ");
                double price; CheckDouble(out price);
                Write("Назва валюти: ");
                string nameCurrency = ReadLine();
                Write("Курс: ");
                double exRate; CheckDouble(out exRate);
                Write("Кількість наявних товарів на складi: ");
                int quantity; CheckInt(out quantity);
                Write("Назва компанії-виробника: ");
                string producer = ReadLine();
                Write("Вага одиниці товару: ");
                double weight; CheckDouble(out weight);
                array[i] = new Product(name, price, nameCurrency, exRate, quantity, producer, weight);
            }
            return array;
        }
        static void PrintTable()
        {
            ForegroundColor = ConsoleColor.White;
            WriteLine("  №  |Назва товару |Ціна |Назва валюти |Курс в грн |Кількість |Компанія - виробник |Вага, кг| ");
            ForegroundColor = ConsoleColor.Blue;
        }
        static void PrintProduct(Product product)
        {
            WriteLine($"{product.GetName(),13}{product.GetPrice(),5}{product.GetCurrencyName(),13}{product.GetCurrencyExRate(),11}{product.GetQuantity(),10}" +
                $"{ product.GetProducer(),20}{ product.GetWeight(),9}");
         }
        static void PrintProducts(Product[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Write($"{i + 1,5}");
                PrintProduct(array[i]);
            }
        }
        static void GetProductsInfo(Product[] array, out int max, out int min)
        {
            min = 0;
            max = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i].GetPrice() > array[max].GetPrice())
                    max = i;
                if (array[i].GetPrice() < array[min].GetPrice())
                    min = i;
            }
        }
        static void SortProductsByPrice(Product[] array)
        {
            double[] arrTmp = new double[array.Length];
            for (int i = 0; i < arrTmp.Length; i++)
                arrTmp[i] = array[i].GetPrice();
            Array.Sort(arrTmp, array);
        }
        static void SortProductsByCount(Product[] array)
        {
            int[] arrTmp = new int[array.Length];
            for (int i = 0; i < arrTmp.Length; i++)
                arrTmp[i] = array[i].GetQuantity();
            Array.Sort(arrTmp, array);
        }
        static int PrintMenu()
        {
            ForegroundColor = ConsoleColor.Blue;
            WriteLine("МЕНЮ");
            WriteLine("Ввести масив продуктів - 1");
            WriteLine("Вивести продукти - 2");
            WriteLine("Сортувати масив - 3");
            WriteLine("Інформація - 4");
            WriteLine("Вийти - 0");
            ForegroundColor = ConsoleColor.White;
            int x; CheckInt(out x, 0, 4);
            return x;
        }
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            OutputEncoding = Encoding.Unicode;
            InputEncoding = Encoding.Unicode;
            Title = "Лабораторна робота №7";
            SetWindowSize(120, 25);
            Product[] array = ReadProductsArray();
            ForegroundColor = ConsoleColor.Gray;
            int check;
            do
            {
                check = PrintMenu();
                switch (check)
                {
                    case 1:
                        array = ReadProductsArray();
                        break;
                    case 2:
                        WriteLine("Усі - 1");
                        WriteLine("Один - 2");
                        WriteLine("Вихід - 0");
                        CheckInt(out check, 0, 2);
                        switch (check)
                        {
                            case 1:
                                Clear();
                                PrintTable();
                                PrintProducts(array);
                                break;
                            case 2:
                                WriteLine("Номер");
                                int x; CheckInt(out x, array.Length);
                                PrintTable();
                                PrintProduct(array[x - 1]);
                                break;
                            case 0:
                                break;
                        }
                        break;
                    case 3:
                        WriteLine("За кількістю - 1");
                        WriteLine("За ціною - 2");
                        WriteLine("Вихід - 0");
                        CheckInt(out check, 0, 2);
                        switch (check)
                        {
                            case 1:
                                Clear();
                                SortProductsByCount(array);
                                break;
                            case 2:
                                SortProductsByPrice(array);
                                break;
                            case 0:
                                break;
                        }
                        PrintTable();
                        PrintProducts(array);
                        break;
                    case 4:
                        Clear();
                        int min, max;
                        GetProductsInfo(array, out max, out min);
                        WriteLine($"Найдорожчий продукт: {array[max].GetName()}: {array[max].GetPrice()}\nНайдешевший: {array[min].GetName()}:{ array[min].GetPrice()}");
                    break;
                    case 0:
                        break;
                }
            } while (check != 0);
        }
    }
}